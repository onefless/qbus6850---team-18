# -*- coding: utf-8 -*-
"""
Created on Thu Sep 27 00:21:13 2018

@author: Francis
"""


import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from scipy.stats.stats import pearsonr
import time
#%% Load data
data=pd.read_csv('ProjectTrain.csv')
bureau = pd.read_csv('ProjectTrain_Bureau.csv')
data=data.merge(bureau,on='SK_ID_CURR')
# Check skewness of all target
print(data['TARGET'].mean()) # 0.07950501072816754
#test = list(data.columns) + list(bureau.columns)
# Examine missing value. 

mis_val_df=pd.DataFrame(data=(data.isnull().sum()/data.shape[0]),index=data.columns,columns=['Missing value %'])
mis_val_df.sort_values('Missing value %',axis=0,ascending=False,inplace=True)
#short-list to missing >10%
mis_col=list(mis_val_df.loc[mis_val_df['Missing value %']>0.1,:].index)


#%%
def kde_plot(col,df):
    plt.figure(figsize = (13, 7))
    sns.kdeplot(df.ix[df['TARGET'] == 0, col], label = 'TARGET == 0')
    sns.kdeplot(df.ix[df['TARGET'] == 1, col], label = 'TARGET == 1')
    plt.xlabel(col); plt.ylabel('Density'); plt.title('%s Distribution' % col)
    plt.legend()
    plt.show()

kde_plot('SK_ID_BUREAU',data)

data.loc[data['AMT_ANNUITY_y'].isnull()==False,'TARGET'].value_counts()#0    32113;1     2895
interm=[]
for i in mis_col:
    j=data.loc[data[i].isnull()==False,'TARGET'].mean()
    interm.append(j)
    
#Only 'AMT_ANNUITY_y', 'OCCUPATION_TYPE' has slightly more +1 data. 

#%%
#Ensemble. Logistic, SVD, random forest, adaboost
data_clean=data.loc[:,data.isnull().mean() < .1]
data_clean=data_clean.dropna(axis=0,how='any')
data_clean=pd.get_dummies(data_clean)
target = data_clean['TARGET']
del data_clean['TARGET']

positive_index=[]#positive classification
for i in target.index:
    if target[i]==1:
        positive_index.append(i)
    
print(len(target.loc[target==1])/target.shape[0])# 0.0795. Very skewed data
print(target.value_counts())# 0 252686;1 21825
Xtr,Xts,Ytr,Yts = train_test_split(data2,target,test_size=0.4,random_state=42)
print('Ytr:',Ytr.mean(),'Yts:',Yts.mean())#Ytr: 0.0798654720302688 Yts: 0.0796517687450187
#%% EDA
print(pearsonr(data2['AMT_CREDIT_SUM'],target))


#%%
#data['NAME_TYPE_SUITE'] = 'unspecified'. Do correlation with +1.
from sklearn.ensemble import AdaBoostClassifier
mdl = AdaBoostClassifier()

stime=time.time()
mdl.fit(data2,target)
print(mdl.feature_importances_)
print(time.time()-stime)#50
features_ada=pd.DataFrame(data=mdl.feature_importances_,index=data2.columns)# Present with normalising 0.18

features_ada_selected =[]
for i,j in zip(features_ada[0],data2.columns):
     if i>0.01:
         features_ada_selected.append(j)
         print(i,j)
''' Ada feature selection
0.08 AMT_CREDIT
0.06 AMT_ANNUITY
0.1 AMT_GOODS_PRICE
0.02 REGION_POPULATION_RELATIVE
0.12 DAYS_BIRTH
0.04 DAYS_EMPLOYED
0.02 DAYS_REGISTRATION
0.04 DAYS_ID_PUBLISH
0.02 FLAG_WORK_PHONE
0.02 REGION_RATING_CLIENT_W_CITY
0.02 REG_CITY_NOT_LIVE_CITY
0.18 EXT_SOURCE_2
0.02 DEF_30_CNT_SOCIAL_CIRCLE
0.04 DAYS_LAST_PHONE_CHANGE
0.02 FLAG_DOCUMENT_3
0.02 FLAG_DOCUMENT_6
0.02 FLAG_DOCUMENT_16
0.02 CODE_GENDER_M
0.02 FLAG_OWN_CAR_Y
0.02 NAME_INCOME_TYPE_Working
0.02 NAME_EDUCATION_TYPE_Higher education
0.02 NAME_FAMILY_STATUS_Married
0.02 ORGANIZATION_TYPE_Business Entity Type 3
0.02 ORGANIZATION_TYPE_Construction
0.02 ORGANIZATION_TYPE_Self-employed
'''



#%%
from sklearn.ensemble import RandomForestClassifier
mdl = RandomForestClassifier(n_estimators=150)
stime=time.time()
mdl.fit(data_clean,target)
features_rf=pd.Series(data=mdl.feature_importances_,index=data_clean.columns)
print(time.time()-stime)#95
features_rf_selected =[]
for i,j in zip(features_rf,data_clean.columns):
     if i>0.01:
         features_rf_selected.append(j)
         print(round(i,4),j)
'''
0.0338 SK_ID_CURR
0.0271 AMT_INCOME_TOTAL
0.0302 AMT_CREDIT
0.0323 AMT_ANNUITY_x
0.0264 AMT_GOODS_PRICE
0.0272 REGION_POPULATION_RELATIVE
0.0356 DAYS_BIRTH
0.0311 DAYS_EMPLOYED
0.0348 DAYS_REGISTRATION
0.0348 DAYS_ID_PUBLISH
0.0112 CNT_FAM_MEMBERS
0.0239 HOUR_APPR_PROCESS_START
0.0513 EXT_SOURCE_2
0.0588 EXT_SOURCE_3
0.014 OBS_30_CNT_SOCIAL_CIRCLE
0.014 OBS_60_CNT_SOCIAL_CIRCLE
0.031 DAYS_LAST_PHONE_CHANGE
0.0185 AMT_REQ_CREDIT_BUREAU_YEAR
0.0346 SK_ID_BUREAU
0.0352 DAYS_CREDIT
0.033 DAYS_CREDIT_ENDDATE
0.0315 AMT_CREDIT_SUM
0.0132 AMT_CREDIT_SUM_DEBT
0.0325 DAYS_CREDIT_UPDATE
'''
#%%

##AMT_CREDIT
#data2['AMT_CREDIT'].plot.hist(title='AMT_CREDIT') 
#plt.scatter(data2['AMT_CREDIT'],target)
##AMT_ANNUITY
#plt.scatter(data2['AMT_ANNUITY'],target)#Vishually significant difference. High value lead to 0.
##AMT_INCOME_TOTAL
#plt.scatter(data2['AMT_INCOME_TOTAL'],target)#Vishually singnicant differnce.
#
#plt.scatter(data2['DAYS_EMPLOYED'],target)


feature_lst=["SK_ID_CURR",
"CNT_CHILDREN",
"AMT_INCOME_TOTAL",
"AMT_CREDIT",
"AMT_ANNUITY",
"AMT_GOODS_PRICE",
"REGION_POPULATION_RELATIVE",
"DAYS_BIRTH",
"DAYS_EMPLOYED",
"DAYS_REGISTRATION",
"DAYS_ID_PUBLISH",
"CNT_FAM_MEMBERS",
"HOUR_APPR_PROCESS_START",
"EXT_SOURCE_2",
"OBS_30_CNT_SOCIAL_CIRCLE",
"OBS_60_CNT_SOCIAL_CIRCLE",
"DAYS_LAST_PHONE_CHANGE",
"AMT_CREDIT",
"AMT_ANNUITY",
"AMT_GOODS_PRICE",
"REGION_POPULATION_RELATIVE",
"DAYS_BIRTH",
"DAYS_EMPLOYED",
"DAYS_REGISTRATION",
"DAYS_ID_PUBLISH",
"FLAG_WORK_PHONE",
"REGION_RATING_CLIENT_W_CITY",
"REG_CITY_NOT_LIVE_CITY",
"EXT_SOURCE_2",
"DEF_30_CNT_SOCIAL_CIRCLE",
"DAYS_LAST_PHONE_CHANGE",
"FLAG_DOCUMENT_3",
"FLAG_DOCUMENT_6",
"FLAG_DOCUMENT_16",
"CODE_GENDER_M",
"FLAG_OWN_CAR_Y",
"NAME_INCOME_TYPE_Working",
"NAME_EDUCATION_TYPE_Higher education",
"NAME_FAMILY_STATUS_Married",
"ORGANIZATION_TYPE_Business Entity Type 3",
"ORGANIZATION_TYPE_Construction",
"ORGANIZATION_TYPE_Self-employed"
]

feature_lst=list(set(feature_lst))

pearson_features,pearson_corr=[],[]
for i in feature_lst:
    corr,p = pearsonr(data2[i],target)
    if p<0.05:
        pearson_features.append(i)
        pearson_corr.append(corr)

test = pd.DataFrame(data=pearson_corr,index=pearson_features)

pearson_features2,pearson_corr2=[],[]
for i in data2.columns:
    corr,p = pearsonr(data2[i],target)
    if p<0.05:
        pearson_features2.append(i)
        pearson_corr2.append(corr)   
test2 = pd.DataFrame(data=pearson_corr2,index=pearson_features2)


