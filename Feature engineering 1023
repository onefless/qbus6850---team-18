
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from scipy.stats.stats import pearsonr
import time
from sklearn.metrics import classification_report

from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import roc_curve
from sklearn.metrics import precision_recall_curve

def classify(pred_prob,threshold):
    prob=pred_prob[:,1]
    result=[]
    for i in range(len(prob)):
        if prob[i]>=threshold:
            result.append(1)
        else:
            result.append(0)
    return result
    
#%% Load data
data=pd.read_csv('ProjectTrain.csv')
bureau = pd.read_csv('ProjectTrain_Bureau.csv')
data=data.merge(bureau,on='SK_ID_CURR',how='left')

data['OCCUPATION_TYPE'].fillna('NOTSPEC',inplace=True)
data['EXT_SOURCE_1'].fillna(data['EXT_SOURCE_1'].mean(),inplace=True)#-0.1007

test_bureau=pd.read_csv('ProjectTest_Bureau.csv')
test_ori=pd.read_csv('ProjectTest.csv')
test_merged=test_bureau.merge(test_ori,on='Index_ID',how='left')


#%% New features
#%% New features
dictionary = {True: 1, False: 0}

test= pd.to_numeric(data['CREDIT_TYPE'] == 'Microloan')#postive rate = 21.6%
test.replace(dictionary)
pearsonr(test,data['TARGET'])
data['Microloan']=test

test=data['CREDIT_TYPE'].isin(['Mortgage','Car loan','Consumer credit'])
test.replace(dictionary)
data['CREDIT_L']=test#0.01675
del data['CREDIT_TYPE']

test=data['ORGANIZATION_TYPE'].isin(['Transport: type 3','Industry: type 1','Construction','Restaurant','Mobile'])
test.replace(dictionary)
data['ORG_HIGH']=test#0.02971

test=data['ORGANIZATION_TYPE'].isin(['Bank','Culture','Military','XNA','Transport: type 1','Trade: type 5',
         'School','Insurance','Industry: type 5'])
test.replace(dictionary)
data['ORG_LOW']=test#-0.0542
del data['ORGANIZATION_TYPE']

test=data['NAME_EDUCATION_TYPE'].isin(['Incomplete higher','Secondary / secondary special','Lower secondary'])
test.replace(dictionary)
data['EDU_HIGH']=test#0.05224

del data['NAME_EDUCATION_TYPE']

#test = data.loc[(data['OCCUPATION_HIGH']==1) & (data['ORG_HIGH']==1),'TARGET']

data['DOC_3less6']=data['FLAG_DOCUMENT_3']-data['FLAG_DOCUMENT_6']

test=data['EXT_SOURCE_2']+data['EXT_SOURCE_3']
data['EXT_SOURCE_2_3']=test
test=data['EXT_SOURCE_2']*data['EXT_SOURCE_3']
data['EXT_SOURCE_2*3']=test

test=data['AMT_ANNUITY_x']*data['AMT_ANNUITY_x']
data['AMT_ANNUITY_x2']=test#(-0.046165101165022926, 2.20525e-129)

test=data['DAYS_EMPLOYED']*data['DAYS_EMPLOYED']
data['DAYS_EMPLOYED2']=test

test=data['EXT_SOURCE_2']*data['EXT_SOURCE_2']
data['EXT_SOURCE_2squ']=test#(-0.1487766159451087, 0.0)

test=data['DAYS_CREDIT'].fillna(0)+data['DAYS_BIRTH']
data['DAYS_CREDIT_BIRTH']=test#(0.08372009048491603, 0.0)

data['AMTcredit/annunity'] = data['AMT_CREDIT']/data['AMT_ANNUITY_x']#-0.0317

data['credsqr/annusqr']=data['AMT_CREDIT']**2/data['AMT_ANNUITY_x']**2 #-0.03600

data['AMT_GOODS_PRICEsqr']=data['AMT_GOODS_PRICE']**2 #-0.04259

data['AMT_ANNUITY_x2_y']=data['AMT_ANNUITY_x']**2-data['AMT_ANNUITY_y'].fillna(0)

test=data['OCCUPATION_TYPE'].isin(['Low-skill Laborers','Drivers','Cooking staff',
         'Waiters/barmen staff','Security staff','Laborers','Cleaning staff','Sales staff'])
test.replace(dictionary)
data['OCCUPATION_HIGH']=test#-0.035309

test=data['OCCUPATION_TYPE'].isin(['Accountants','HR staff','Core staff','Managers'])
test.replace(dictionary)
data['OCCUPATION_LOW']=test#0.073419
del data['OCCUPATION_TYPE'] # !!!

data['ext_sources_mean']=data[['EXT_SOURCE_1', 'EXT_SOURCE_2', 'EXT_SOURCE_3']].mean(axis=1)#-0.2295
data['credit_to_goods_ratio'] = data['AMT_CREDIT'] / data['AMT_GOODS_PRICE']#0.067456
del data[['EXT_SOURCE_1', 'EXT_SOURCE_2', 'EXT_SOURCE_3']]
#data['DAYS_BIRTH_new']=(data['DAYS_BIRTH']-data['DAYS_BIRTH'].mean())/np.std(data['DAYS_BIRTH'])

#data['test']=data['DAYS_BIRTH_new']-(data['DAYS_EMPLOYED']-data['DAYS_EMPLOYED'].mean())/np.std(data['DAYS_EMPLOYED'])

#parameters = {'n_neighbors':[i for i in range(51,101,2)]}
#estimator = KNeighborsClassifier()
#clf=GridSearchCV(estimator,parameters,cv=5)
knn_data=data.loc[:,['ext_sources_mean','AMTcredit/annunity','OCCUPATION_LOW','OCCUPATION_HIGH']].dropna(axis=0,how='any')
#clf.fit(knn_data,data.loc[data.loc[:,['EXT_SOURCE_3', 'EXT_SOURCE_2','AMTcredit/annunity']].dropna(axis=0,how='any').index,'TARGET'])
#clf.best_params_

mdl = KNeighborsClassifier(n_neighbors = 55, weights='uniform')
mdl.fit(knn_data,data.loc[data.loc[:,['ext_sources_mean','AMTcredit/annunity','OCCUPATION_LOW','OCCUPATION_HIGH']].dropna(axis=0,how='any').index,'TARGET'])
knn_prob=mdl.predict_proba(knn_data)
knn_prob_df=pd.DataFrame(data=knn_prob[:,1],columns =['KNN_PROB'], index=data.loc[:,['EXT_SOURCE_3', 'EXT_SOURCE_2','AMTcredit/annunity']].dropna(axis=0,how='any').index)
data=data.merge(knn_prob_df,how='inner',left_index=True,right_index =True)
data['KNN_PROB'].fillna(knn_prob.mean(),inplace=True) #0.3114

pearsonr(data['KNN_PROB'].fillna(0),data['TARGET'])

#%% Data cleaning for train test split
data_clean=data.loc[:,data.isnull().mean() < .21]
data_clean=data_clean.dropna(axis=0,how='any')
data_clean=pd.get_dummies(data_clean)
target = data_clean['TARGET']
del data_clean['TARGET']
data_clean.drop(columns=['SK_ID_CURR'],inplace=True)

Xtr,Xts,Ytr,Yts = train_test_split(data_clean,target,test_size=0.4,random_state=42)
